import express from "express";
import { uploader } from "../helpers.js";

export default (app) => {
  const router = express.Router();

  router.post("/file", uploader.single("file"), (req, res, next) => {
    res.send("ok!");
  });

  router.post(
    "/files",
    uploader.array("multiple_files", 12),
    (req, res, next) => {
      res.send("ok!");
    }
  );

  app.use(router);
};
