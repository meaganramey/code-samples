import { Sequelize, Model, DataTypes } from "sequelize";

const sequelize = new Sequelize("students", "postgres", "password", {
  host: "localhost",
  dialect: "postgres",
});

const User = sequelize.define("user", {
  name: DataTypes.TEXT,
  favoriteColor: {
    type: DataTypes.TEXT,
    defaultValue: "purple",
  },
  age: DataTypes.INTEGER,
  cash: DataTypes.INTEGER,
});

async function start() {
  await sequelize.sync({ force: process.env.NODE_ENV !== "production" });

  try {
    const vince = await User.create({
      name: "Vince",
      age: "29",
      cash: 200000000,
    });

    const andre = await User.create({
      name: "Andre 300",
      favoriteColor: "magenta",
      age: 175,
      cash: 87326823,
    });

    // const allUsers = await User.findAll({
    //   attributes: ["name", "favoriteColor"],
    //   limit: 5,
    // });
    const anotherVince = await User.findByPk(1);

    console.log({ anotherVince });

    // vince exists in the database now!
    // console.log(vince instanceof User); // true
    // console.log(vince.name); // "vince"
  } catch (err) {
    console.log("No explosion???", err);
  }
}

start();
