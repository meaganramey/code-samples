import { MongoClient } from "mongodb";
import util from "util";

const connectToMongo = util.promisify(MongoClient.connect);

async function start() {
  const client = await connectToMongo("mongodb://localhost:27017");

  const studentDB = client.db("students");

  const cursor = studentDB.collection("student").find({});

  cursor.forEach((student) => {
    console.log({ student });
  });

  client.close();
}

try {
  start();
} catch (err) {
  console.error("Ooops", err);
}

// username: boss
// password: bosspassword
// mongodb+srv://boss:bosspassword@cluster0.61qx6.mongodb.net/test
