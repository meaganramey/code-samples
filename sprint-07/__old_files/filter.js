const people = [
  {
    name: "Greg Hodges",
    active: "true",
    likesFood: "false",
    id: "8e6aaa52-a541-49b3-be59-0204c1b0ec24",
  },
  {
    name: "Jacob Walker",
    active: "true",
    likesFood: "true",
    id: "2b9dca52-758b-473c-a4fe-b3ed2c059bde",
  },
  {
    name: "Arianna Gabby",
    active: "true",
    likesFood: "true",
    id: "fa8ad45b-ab4b-4a55-94b2-33ca015d9780",
  },
  {
    name: "Vincent St. Louis",
    active: "false",
    likesFood: "false",
    id: "15243ebb-9b85-4a86-b757-c5fa8c5d5b8a",
  },
  {
    name: "Bethsheba Z",
    active: "false",
    likesFood: "true",
    id: "37e966b2-37af-459a-9673-a242e5928a3d",
  },
];

const lodashFilterExample = (arr, params = {}) => {
  const paramNames = Object.keys(params);
  const paramLength = paramNames.length;
  return arr.filter((item) => {
    // loop over the params
    for (let i = 0; i < paramLength; i++) {
      const currentParam = paramNames[i];
      if (
        // if the item does not have a key of this param
        typeof item[currentParam] === undefined ||
        // or if the items key of this param does not match the params value
        item[currentParam] !== params[currentParam]
      ) {
        return false;
      }
    }
    // if we made it this far it works
    return true;
  });
};

console.log(lodashFilterExample(people, { active: "true", likesFood: "true" }));
