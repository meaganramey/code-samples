import * as BPromise from "bluebird";
import axios from "axios";

import * as env from "./env";

BPromise.config({
  // Enable warnings
  warnings: true,
  // Enable long stack traces
  longStackTraces: true,
  // Enable cancellation
  cancellation: true,
  // Enable monitoring
  monitoring: true,
  // Enable async hooks
  asyncHooks: true,
});

export const BBPromise = BPromise;

export const delay = (ms = 2000) => new BBPromise((resolve) => setTimeout(resolve, ms));

export async function getWeather(locationData) {
  // console.log(`
  //   Retrieving the weather from ${env.WEATHER_API_URL}
  //   with a key of ${env.WEATHER_API_KEY}
  // `);

  if (!locationData) {
    return null;
  }

  const { data } = await axios.get(`${env.WEATHER_API_URL}/current.json`, {
    params: {
      key: env.WEATHER_API_KEY,
      q: `${locationData.latitude},${locationData.longitude}`,
    },
  });

  return data;
}
