import React from "react";
import Counter from "./Counter";
import Swapi from "./Swapi";

class App extends React.PureComponent {
  render() {
    return (
      <>
        <Counter />
        <br />
        <br />
        <br />
        <br />
        <br />
        <Swapi />
      </>
    );
  }
}

export default App;
