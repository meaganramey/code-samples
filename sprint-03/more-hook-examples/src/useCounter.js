import { useState, useEffect } from "react";

export default function useCounter(initialValue = 0) {
  const [count, updateCount] = useState(initialValue);

  const increaseCounter = () => {
    updateCount((state) => state + 1);
  };

  const decreaseCounter = () => {
    updateCount((state) => state - 1);
  };

  return {
    count,
    updateCount,
    increaseCounter,
    decreaseCounter,
  };
}

export const useCoolCounter = (initialValue) => {
  const counter = useCounter(initialValue);

  useEffect(() => {
    let timerId = setInterval(() => {
      counter.increaseCounter();
    }, 1000);

    return () => {
      clearInterval(timerId);
    };
  });

  const reduceCoolFactor = (num) => {
    counter.updateCount((cf) => cf - num);
  };

  return {
    ...counter,
    reduceCoolFactor,
  };
};
