import * as firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/analytics'
  
const firebaseConfig = {
    apiKey: "AIzaSyBfFjbIqxQjg0G4UsgoVucah2cKjzCtXEs",
    authDomain: "fir-auth-demo-8d7dd.firebaseapp.com",
    databaseURL: "https://fir-auth-demo-8d7dd.firebaseio.com",
    projectId: "fir-auth-demo-8d7dd",
    storageBucket: "fir-auth-demo-8d7dd.appspot.com",
    messagingSenderId: "288322752077",
    appId: "1:288322752077:web:bf78f08956c8981fc51604",
    measurementId: "G-14XN3JBCBB"
};

// Initialize Firebase

export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();
firebase.analytics();