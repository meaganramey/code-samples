import React from "react";
import { Segment } from "@fluentui/react-northstar";

export const Content = ({ children, leftComponent }) => (
  <>
    <Segment
      color="white"
      content="Menu"
      inverted
      styles={{
        gridColumn: "span 1",
      }}
    >
      {leftComponent}
    </Segment>
    <Segment
      content="Content"
      styles={{
        gridColumn: "span 3",
      }}
    >
      {children}
    </Segment>
  </>
);
