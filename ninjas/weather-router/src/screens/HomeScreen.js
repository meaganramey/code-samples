import React from "react";

export const HomeScreen = ({ locations }) => {
  return (
    <>
      <h1>Showing All of the Locations</h1>
      <ol>
        {locations.map((loc) => (
          <li key={loc.id}>{loc.place}</li>
        ))}
      </ol>
    </>
  );
};
